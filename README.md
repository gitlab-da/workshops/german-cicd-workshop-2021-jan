# German CI/CD Workshop 2021

## Resources

- [Event & Recording](https://learn.gitlab.com/german-connect-april-2021/watch-31)
- [Slides](https://docs.google.com/presentation/d/1n0zykCpPK22f9fsGhbAy2rrYEZq-0XU_1GmlfEVa7n0/edit?usp=sharing)
- [Organisation](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1976)

The slides provide the step-by-step instructions as exercises for this repository.

- CI/CD Getting Started
- Security scanning
